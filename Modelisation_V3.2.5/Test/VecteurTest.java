import static org.junit.Assert.assertEquals;
import gestion_figure.Vecteur;

import org.junit.Test;

public class VecteurTest {

	@Test
	public void testProduitVectoriel() {
		Vecteur v1 = new Vecteur(2.0, 2.0, 2.0);
		Vecteur v2 = new Vecteur(1.0, 1.0, 1.0);

		Vecteur test = v1.produitVectoriel(v2);

		assertEquals(0.0, test.getX(), 0.00001);
		assertEquals(0.0, test.getY(), 0.00001);
		assertEquals(0.0, test.getZ(), 0.00001);

		v2 = new Vecteur(1.5, 1.5, 1.5);
		test = v1.produitVectoriel(v2);
		assertEquals(0.0, test.getX(), 0.00001);
		assertEquals(0.0, test.getY(), 0.00001);
		assertEquals(0.0, test.getZ(), 0.00001);

		v2 = new Vecteur(2, 3, 1);
		test = v1.produitVectoriel(v2);
		assertEquals(-4, test.getX(), 0.00001);
		assertEquals(2, test.getY(), 0.00001);
		assertEquals(2, test.getZ(), 0.00001);
	}

	@Test
	public void testProduitScalaire() {
		Vecteur v1 = new Vecteur(2.0, 2.0, 2.0);
		Vecteur v2 = new Vecteur(1.0, 1.0, 1.0);

		double test = v1.produitScalaire(v2);
		assertEquals(6, test, 0.00001);

		v2 = new Vecteur(1.5, 1.5, 1.5);
		test = v1.produitScalaire(v2);
		assertEquals(9, test, 0.00001);

		v2 = new Vecteur(2, 3, 1);
		test = v1.produitScalaire(v2);
		assertEquals(12, test, 0.00001);

	}

	@Test
	public void testNorme() {
		Vecteur v = new Vecteur(0, 0, 0);
		double norme = v.getNorme();
		assertEquals(0, norme, 0.001);

		v = new Vecteur(1, 1, 1);
		norme = v.getNorme();
		assertEquals(Math.sqrt(3), norme, 0.001);

		v = new Vecteur(2, 3, 4);
		norme = v.getNorme();
		assertEquals(Math.sqrt(29), norme, 0.001);
	}

}
