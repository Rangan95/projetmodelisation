package Threads;

import gestion_figure.Panel_Figure;

/**
 * cette classe permet de gerer les threads, c'est a dire les actions automatiques
 * @author remy
 *
 */

public class ThreadRotation extends Thread {
	private Panel_Figure panelFigure;
	private int time;

	public ThreadRotation(Panel_Figure panelFigure){
		this.panelFigure = panelFigure;
		if(this.panelFigure.getFigure().getPoint().length<15000)
			time = 50;
		else 
			time = 500;
	}

	@Override
	public void run() {
		while(this.panelFigure.getFigure().isRotationAuto()){
			try {
				Thread.sleep(time);
				panelFigure.getFigure().rotationX(0.01);
				panelFigure.getFigure().rotationY(0.01);
				panelFigure.repaint();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
