package loading;

import gestion_figure.Face;
import gestion_figure.Figure;
import gestion_figure.Point;
import gestion_figure.Segment;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

import javax.swing.JOptionPane;

/**
 * Permet de charger un fichier gts
 * @author remy
 */

public class LoadFigure{
	public Figure creerFigure(String chemin) {
		//On vérifie que le format est valide
		if(!chemin.substring(chemin.length()-4).equals(".gts")){
			JOptionPane.showMessageDialog(null, "Le fichier n'est pas un fichier gts");
		}else{

			Scanner s;
			try{
				s = new Scanner(new FileReader(chemin));
				Point[] pts = new Point[Integer.parseInt(s.next())];
				Segment[] seg = new Segment[Integer.parseInt(s.next())];
				Face[] tr = new Face[Integer.parseInt(s.next())];

				int curseurPoints =0;
				int curseurSegments = 0;
				int curseurTriangle = 0;

				while(s.hasNext()){
					if(curseurPoints<pts.length){
						pts[curseurPoints++] = new Point(Double.parseDouble(s.next()),Double.parseDouble(s.next()),Double.parseDouble(s.next()));
					}
					else if(curseurSegments<seg.length){
						seg[curseurSegments++] = new Segment(pts[Integer.parseInt(s.next())-1],pts[Integer.parseInt(s.next())-1]);
					}
					else if(curseurTriangle<tr.length){
						tr[curseurTriangle++]= new Face(seg[Integer.parseInt(s.next())-1],seg[Integer.parseInt(s.next())-1], seg[Integer.parseInt(s.next())-1]);
					}
				}
				s.close();
				return new Figure(pts,seg, tr);	

			}catch (FileNotFoundException e) {
				JOptionPane.showMessageDialog(null, "Erreur figure non trouvée");
				e.printStackTrace();
			}
			catch (Exception e){
				JOptionPane.showMessageDialog(null, "Erreur dans le chargement de la figure");
				e.printStackTrace();
			}
		}
		return null;
	}
}