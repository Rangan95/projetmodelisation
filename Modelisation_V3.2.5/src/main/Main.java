package main;

import javax.swing.UIManager;

import fenetre.Fenetre;

public class Main{
	public static void main(String[] args){
		/**
		 * On met un style a la fenetre
		 */
		try{
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		new Fenetre();
	}
}