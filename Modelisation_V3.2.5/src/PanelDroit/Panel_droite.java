package PanelDroit;

import fenetre.Fenetre;

import java.awt.Dimension;

import javax.swing.JPanel;

/**
 * Cette classe regroupe tous les composants du panel de droite(qui gere la figure)
 * @author remy
 *
 */

public class Panel_droite extends JPanel {
	private Onglet_panel_droite onglet;
	
	public Panel_droite(Fenetre fen){
		this.onglet = new Onglet_panel_droite(fen);
		this.setPreferredSize(new Dimension(300, 600));
		this.add(onglet);
	}
	
	public Onglet_panel_droite getOngletsPanelDroit(){
		return onglet;
	}
}
