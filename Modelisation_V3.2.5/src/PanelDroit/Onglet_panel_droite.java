package PanelDroit;

import java.awt.Dimension;

import fenetre.Fenetre;

import javax.swing.JTabbedPane;

import PanelDeplacement.Panel_Deplacement;
import PanelEdition.Panel_Edition;

/**
 * Cette classe permet de gerer les onglets du panel de droite
 * @author remy
 *
 */

public class Onglet_panel_droite extends JTabbedPane {
	private Panel_Deplacement panelDeplacement;
	private Panel_Edition panelEdition;
	
	public Onglet_panel_droite(Fenetre fen){
		this.setPreferredSize(new Dimension(290, 630));
		this.panelDeplacement = new Panel_Deplacement(fen);
		this.panelEdition = new Panel_Edition(fen);
		this.addTab("Deplacement", panelDeplacement);
		this.addTab("Edition", panelEdition);
	}
	
	public Panel_Deplacement getPanDeplacement(){
		return panelDeplacement;
	}
	
	public Panel_Edition getPanEdition(){
		return panelEdition;
	}
}
