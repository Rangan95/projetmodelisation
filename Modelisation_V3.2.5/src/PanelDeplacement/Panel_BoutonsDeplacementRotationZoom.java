package PanelDeplacement;

import fenetre.Fenetre;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Cette classe permet de gerer les boutons qui effectue une action sur la figure
 * @author remy
 *
 */

public class Panel_BoutonsDeplacementRotationZoom extends JPanel implements ActionListener {
	private JButton deplacement_droite = new JButton(">");
	private JButton deplacement_gauche = new JButton("<");
	private JButton deplacement_haut = new JButton("^");
	private JButton deplacement_bas = new JButton("v");
	private JButton rotation_droite = new JButton(">");
	private JButton rotation_gauche = new JButton("<");
	private JButton rotation_haut = new JButton("^");
	private JButton rotation_bas = new JButton("v");

	private JButton zoomPlus = new JButton("+");
	private JButton zoomMoins = new JButton("-");

	private JButton boutonRotaSensiPlus = new JButton("+");
	private JButton boutonRotaSensiMoins = new JButton("-");
	private JButton boutonTransSensiPlus = new JButton("+");
	private JButton boutonTransSensiMoins = new JButton("-");

	private JPanel pan_deplacement = new JPanel();
	private JPanel pan_rotation = new JPanel();
	private JPanel pan_zoomPrincipale = new JPanel();
	private JPanel pan_zoomBouton = new JPanel();
	private JPanel pan_sensi = new JPanel();

	private JLabel lab_Deplacer = new JLabel("Deplacer");
	private JLabel lab_Rotation = new JLabel("Rotation");

	private JLabel lab_zoom = new JLabel("Zoomer");

	private JLabel lab_RotaSensi = new JLabel("Rotation sensi :     ");
	private JLabel lab_TransSensi = new JLabel("Translation sensi : ");

	private Box boxPrincipale = Box.createVerticalBox();
	private Box boxBoutons = Box.createHorizontalBox();
	private Box boxLabelBoutonDeplacement = Box.createHorizontalBox();
	private Box boxSensi = Box.createVerticalBox();

	private Fenetre fen;

	public Panel_BoutonsDeplacementRotationZoom(Fenetre fen){
		this.fen = fen;

		this.setPreferredSize(new Dimension(200, 400));
		
		this.boutonRotaSensiMoins.setEnabled(false);
		this.boutonRotaSensiPlus.setEnabled(false);
		this.boutonTransSensiMoins.setEnabled(false);
		this.boutonTransSensiPlus.setEnabled(false);
		this.zoomMoins.setEnabled(false);
		this.zoomPlus.setEnabled(false);
		this.rotation_bas.setEnabled(false);
		this.rotation_droite.setEnabled(false);
		this.rotation_gauche.setEnabled(false);
		this.rotation_haut.setEnabled(false);
		this.deplacement_bas.setEnabled(false);
		this.deplacement_droite.setEnabled(false);
		this.deplacement_gauche.setEnabled(false);
		this.deplacement_haut.setEnabled(false);

		this.setPanBouton();
		this.setPanZoom();
		this.setPanSensi();
		this.addListener();
		this.boutonRotaSensiMoins.addActionListener(this);
		this.boutonRotaSensiPlus.addActionListener(this);
		this.boutonTransSensiMoins.addActionListener(this);
		this.boutonTransSensiPlus.addActionListener(this);

		this.boxBoutons.add(pan_deplacement);
		this.boxBoutons.add(Box.createRigidArea(new Dimension(10, 0)));
		this.boxBoutons.add(pan_rotation);
		this.boxLabelBoutonDeplacement.add(lab_Deplacer);
		this.boxLabelBoutonDeplacement.add(Box.createRigidArea(new Dimension(40, 0)));
		this.boxLabelBoutonDeplacement.add(lab_Rotation);


		this.boxPrincipale.add(Box.createRigidArea(new Dimension(0, 40)));
		this.boxPrincipale.add(boxBoutons);
		this.boxPrincipale.add(boxLabelBoutonDeplacement);
		this.boxPrincipale.add(Box.createRigidArea(new Dimension(0, 40)));
		this.boxPrincipale.add(pan_zoomPrincipale);
		this.boxPrincipale.add(Box.createRigidArea(new Dimension(0, 40)));
		this.boxPrincipale.add(pan_sensi);
		this.add(boxPrincipale);
	}
	
	/**
	 * Permet de remettre tous les boutons cliquables
	 */
	
	public void setBouton(){
		this.boutonRotaSensiMoins.setEnabled(true);
		this.boutonRotaSensiPlus.setEnabled(true);
		this.boutonTransSensiMoins.setEnabled(true);
		this.boutonTransSensiPlus.setEnabled(true);
		this.zoomMoins.setEnabled(true);
		this.zoomPlus.setEnabled(true);
		this.rotation_bas.setEnabled(true);
		this.rotation_droite.setEnabled(true);
		this.rotation_gauche.setEnabled(true);
		this.rotation_haut.setEnabled(true);
		this.deplacement_bas.setEnabled(true);
		this.deplacement_droite.setEnabled(true);
		this.deplacement_gauche.setEnabled(true);
		this.deplacement_haut.setEnabled(true);
	}
	
	/**
	 * Cette fonction permet de mettre les listeners sur les boutons
	 */

	public void addListener(){
		this.deplacement_bas.addActionListener(this);
		this.deplacement_droite.addActionListener(this);
		this.deplacement_gauche.addActionListener(this);
		this.deplacement_haut.addActionListener(this);
		this.rotation_bas.addActionListener(this);
		this.rotation_droite.addActionListener(this);
		this.rotation_gauche.addActionListener(this);
		this.rotation_haut.addActionListener(this);
		this.zoomMoins.addActionListener(this);
		this.zoomPlus.addActionListener(this);
	}
	
	/**
	 * Cette fonction permet de mettre a jour le panel de sensi
	 */

	public void setPanSensi(){
		JPanel panBoutonRota = new JPanel();
		JPanel panBoutonTrans = new JPanel();
		Box boxTrans = Box.createHorizontalBox();
		Box boxRota = Box.createHorizontalBox();

		panBoutonRota.setLayout(new BorderLayout());
		panBoutonTrans.setLayout(new BorderLayout());

		panBoutonTrans.add(boutonTransSensiPlus, BorderLayout.NORTH);
		panBoutonTrans.add(boutonTransSensiMoins, BorderLayout.SOUTH);
		panBoutonRota.add(boutonRotaSensiPlus, BorderLayout.NORTH);
		panBoutonRota.add(boutonRotaSensiMoins, BorderLayout.SOUTH);

		boxTrans.add(lab_TransSensi);
		boxTrans.add(panBoutonTrans);

		boxRota.add(lab_RotaSensi);
		boxRota.add(panBoutonRota);

		this.boxSensi.add(boxTrans);
		this.boxSensi.add(boxRota);

		this.pan_sensi.add(boxSensi);
	}
	
	/**
	 * Cette fonction permet de mettre a jour le panel de zoom
	 */

	public void setPanZoom(){
		pan_zoomBouton.setLayout(new BorderLayout());
		this.pan_zoomBouton.add(zoomPlus, BorderLayout.NORTH);
		this.pan_zoomBouton.add(zoomMoins, BorderLayout.SOUTH);

		this.pan_zoomPrincipale.add(lab_zoom);
		this.pan_zoomPrincipale.add(pan_zoomBouton);
	}
	
	/**
	 * Permet de mettre a jour le panel de boutons
	 */

	public void setPanBouton(){
		pan_deplacement.setLayout(new BorderLayout());
		this.pan_deplacement.add(deplacement_droite, BorderLayout.EAST);
		this.pan_deplacement.add(deplacement_gauche, BorderLayout.WEST);
		this.pan_deplacement.add(deplacement_haut, BorderLayout.NORTH);
		this.pan_deplacement.add(deplacement_bas, BorderLayout.SOUTH);

		pan_rotation.setLayout(new BorderLayout());
		this.pan_rotation.add(rotation_droite, BorderLayout.EAST);
		this.pan_rotation.add(rotation_gauche, BorderLayout.WEST);
		this.pan_rotation.add(rotation_haut, BorderLayout.NORTH);
		this.pan_rotation.add(rotation_bas, BorderLayout.SOUTH);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(this.fen.getPanelFigure() != null){
			if(this.fen.getPanelFigure().getFigure().isZoomRestreint() == false){
				if(e.getSource() == zoomPlus){
					this.fen.getPanelFigure().getFigure().zoom(1.1);
					this.fen.getPanelFigure().repaint();
				}
				if(e.getSource() == zoomMoins){
					this.fen.getPanelFigure().getFigure().zoom(0.9);
					this.fen.getPanelFigure().repaint();
				}
			}
			if(this.fen.getPanelFigure().getFigure().isRestreindreDeplacement() == false){
				if(e.getSource() == deplacement_bas){
					this.fen.getPanelFigure().getFigure().translation(0, this.fen.getPanelFigure().getFigure().getTransSensi());
					this.fen.getPanelFigure().repaint();
				}
				if(e.getSource() == deplacement_droite){
					this.fen.getPanelFigure().getFigure().translation(this.fen.getPanelFigure().getFigure().getTransSensi(), 0);
					this.fen.getPanelFigure().repaint();
				}
				if(e.getSource() == deplacement_gauche){
					this.fen.getPanelFigure().getFigure().translation(-this.fen.getPanelFigure().getFigure().getTransSensi(), 0);
					this.fen.getPanelFigure().repaint();
				}
				if(e.getSource() == deplacement_haut){
					this.fen.getPanelFigure().getFigure().translation(0, -this.fen.getPanelFigure().getFigure().getTransSensi());
					this.fen.getPanelFigure().repaint();
				}
				if(e.getSource() == rotation_bas){
					this.fen.getPanelFigure().getFigure().rotationX(-this.fen.getPanelFigure().getFigure().getRotationSensi());
					this.fen.getPanelFigure().repaint();
				}
				if(e.getSource() == rotation_droite){
					this.fen.getPanelFigure().getFigure().rotationY(-this.fen.getPanelFigure().getFigure().getRotationSensi());
					this.fen.getPanelFigure().repaint();
				}
				if(e.getSource() == rotation_gauche){
					this.fen.getPanelFigure().getFigure().rotationY(this.fen.getPanelFigure().getFigure().getRotationSensi());
					this.fen.getPanelFigure().repaint();
				}
				if(e.getSource() == rotation_haut){
					this.fen.getPanelFigure().getFigure().rotationX(this.fen.getPanelFigure().getFigure().getRotationSensi());
					this.fen.getPanelFigure().repaint();
				}
			}
			if(e.getSource() == boutonRotaSensiPlus && this.fen.getPanelFigure().getFigure().getRotationSensi() < 0.5){
				this.fen.getPanelFigure().getFigure().setRotationSensi(this.fen.getPanelFigure().getFigure().getRotationSensi() + 0.1);
			}
			if(e.getSource() == boutonRotaSensiMoins && this.fen.getPanelFigure().getFigure().getRotationSensi() > 0.2){
				this.fen.getPanelFigure().getFigure().setRotationSensi(this.fen.getPanelFigure().getFigure().getRotationSensi() - 0.1);
			}
			if(e.getSource() == boutonTransSensiPlus){
				this.fen.getPanelFigure().getFigure().setTransSensi(this.fen.getPanelFigure().getFigure().getTransSensi()+3);
			}
			if(e.getSource() == boutonTransSensiMoins && this.fen.getPanelFigure().getFigure().getTransSensi() > 3){
				this.fen.getPanelFigure().getFigure().setTransSensi(this.fen.getPanelFigure().getFigure().getTransSensi()-3);
			}
		}
	}	
}
