package PanelDeplacement;

import fenetre.Fenetre;

import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.JPanel;

/**
 * Cette classe permet de gerer la panel de deplacement(c'est a dire tout ce qui touche a la gestion de la figure)
 * @author remy
 *
 */

public class Panel_Deplacement extends JPanel {
	private Panel_BoutonsDeplacementRotationZoom pan_bdr;
	private Panel_ActionFigure pan_af;
	private Box box = Box.createVerticalBox();
	
	public Panel_Deplacement(Fenetre fen){
		this.pan_af = new Panel_ActionFigure(this, fen);
		this.pan_bdr = new Panel_BoutonsDeplacementRotationZoom(fen);
		this.setPreferredSize(new Dimension(300, 610));
		box.add(pan_bdr);
		box.add(pan_af);
		this.add(box);
	}
	
	public Panel_BoutonsDeplacementRotationZoom getPanBdr(){
		return pan_bdr;
	}
	
	public Panel_ActionFigure getPanAction(){
		return this.pan_af;
	}
}
