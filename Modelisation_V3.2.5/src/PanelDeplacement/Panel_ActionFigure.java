package PanelDeplacement;

import fenetre.Fenetre;

import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import Threads.ThreadRotation;

/**
 * Cette classe permet de gerer les actions sur la figure
 * @author remy
 *
 */

public class Panel_ActionFigure extends JPanel implements ItemListener {
	private JRadioButton restreindreDep = new JRadioButton();
	private JRadioButton rotationAuto = new JRadioButton();
	private JRadioButton zoomParDefaut = new JRadioButton();
	private JLabel labCheckRestDep = new JLabel("Restreindre dep");
	private JLabel labCheckRotaAuto = new JLabel("Rotation Auto");
	private JLabel labCheckZoom = new JLabel("Zoom restreint");
	private Box boxCheck1 = Box.createHorizontalBox();
	private Box boxCheck2 = Box.createHorizontalBox();
	private Box boxCheck3 = Box.createHorizontalBox();
	private Box boxPrincipale = Box.createVerticalBox();
	private Panel_Deplacement pan_dep;
	private Fenetre fen;
	private ThreadRotation threadRotation;

	public Panel_ActionFigure(Panel_Deplacement pan_dep, Fenetre fen){
		this.fen = fen;
		this.pan_dep = pan_dep;
		
		this.restreindreDep.setEnabled(false);
		this.rotationAuto.setEnabled(false);
		this.zoomParDefaut.setEnabled(false);
		
		rotationAuto.addItemListener(this);
		restreindreDep.addItemListener(this);
		zoomParDefaut.addItemListener(this);
		
		boxPrincipale.add(Box.createRigidArea(new Dimension(0, 100)));
		boxCheck1.add(restreindreDep);
		boxCheck1.add(labCheckRestDep);
		boxCheck2.add(rotationAuto);
		boxCheck2.add(labCheckRotaAuto);
		boxCheck3.add(zoomParDefaut);
		boxCheck3.add(labCheckZoom);
		boxPrincipale.add(boxCheck1);
		boxPrincipale.add(boxCheck2);
		boxPrincipale.add(boxCheck3);
		this.add(boxPrincipale);
	}
	
	/**
	 * Permet de remettre les boutons radio cliquable
	 */
	
	public void setRadio(){
		this.restreindreDep.setEnabled(true);
		this.rotationAuto.setEnabled(true);
		this.zoomParDefaut.setEnabled(true);
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		if(e.getSource() == rotationAuto && this.fen.getPanelFigure().getFigure() != null){
			if(this.fen.getPanelFigure() != null){
				this.fen.getPanelFigure().getFigure().setRotationAuto(!this.fen.getPanelFigure().getFigure().isRotationAuto());
				threadRotation = new ThreadRotation(this.fen.getPanelFigure());
				threadRotation.start();
			}
		}
		if(e.getSource() == restreindreDep && this.fen.getPanelFigure().getFigure() != null){
			this.fen.getPanelFigure().getFigure().setRestreindreDeplacement(!this.fen.getPanelFigure().getFigure().isRestreindreDeplacement());
		}
		if(e.getSource() == zoomParDefaut && this.fen.getPanelFigure().getFigure() != null){
			this.fen.getPanelFigure().getFigure().setZoomRestreint(!this.fen.getPanelFigure().getFigure().isZoomRestreint());
		}
	}
}
