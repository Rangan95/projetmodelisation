package fenetre;

import gestion_figure.Face;
import gestion_figure.Figure;
import gestion_figure.Point;
import gestion_figure.Segment;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JToolBar;

import bdd.Connexion;

/**
 * Cette classe permet de gerer la barre d'outil
 * @author remy
 *
 */

public class ToolBar extends JToolBar implements ActionListener{
	private JButton save = new JButton();//a coder
	private JButton open = new JButton();
	private JButton intensitePlus = new JButton();
	private JButton intensiteMoins = new JButton();
	private Fenetre fen;

	public ToolBar(Fenetre fen){
		this.fen = fen;
		this.initToolBar();
	}

	/**
	 * Permet d'initialiser la Barre d'outil
	 */

	private void initToolBar() {
		this.setBackground(new Color(200,200,200));
		this.initButton("../Icones/save.png", save);
		this.initButton("../Icones/open.png", open);
		this.initButton("../Icones/intensite+.png", intensitePlus);
		this.initButton("../Icones/intensite-.png", intensiteMoins);

		this.intensiteMoins.setEnabled(false);
		this.intensitePlus.setEnabled(false);

		save.setToolTipText("Sauvegarder");
		open.setToolTipText("Importer");
		intensitePlus.setToolTipText("Lumiere +");
		intensiteMoins.setToolTipText("Lumiere -");
	}

	/**
	 * Permet de remettre les boutons en marche
	 */

	public void setBouton(){
		this.intensiteMoins.setEnabled(true);
		this.intensitePlus.setEnabled(true);
	}

	/**
	 * Permet de modifier le disign d'un bouton
	 * @param cheminImage
	 * @param bouton
	 */

	private void initButton(String cheminImage, JButton bouton){
		bouton.setIcon(new ImageIcon(cheminImage));
		bouton.setBorderPainted(false);
		bouton.addActionListener(this);
		this.add(bouton);
	}

	/**
	 * permet d'enregistrer une figure dans un fichier
	 * @param chemin
	 * @param nom
	 */

	private void enregistrer_gts(String chemin, String nom) {
		Connexion con = new Connexion("ProjetMode.db");	
		ResultSet rs;
		int cpt = 1;
		con.connect();
		
		//on met try si jamais il y a une exception
		try
		{
			/**
			 * BufferedWriter a besoin d un FileWriter, 
			 * les 2 vont ensemble, on donne comme argument le nom du fichier
			 * true signifie qu on ajoute dans le fichier (append), on ne marque pas par dessus
			 */
			FileWriter fw = new FileWriter(nom, true);

			// le BufferedWriter output auquel on donne comme argument le FileWriter fw cree juste au dessus
			BufferedWriter output = new BufferedWriter(fw);
			
			rs = con.querySelect("Select chemin from figures where nom = '" + nom + "'");

			Scanner s = null;
			try {
				s = new Scanner(new FileReader(rs.getString("chemin")));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally{
				while(s.hasNext()){
					output.write(s.next() + " ");
					//on peut utiliser plusieurs fois methode write

					output.flush();
					//ensuite flush envoie dans le fichier, ne pas oublier cette methode pour le BufferedWriter
					if(cpt % 3 == 0){
						output.newLine();
					}
					cpt++;
				}

				s.close();

				output.close();
				//et on le ferme
				JOptionPane.showMessageDialog(null, "Enregistré");
			}
		}
		catch(IOException ioe){
			System.out.print("Erreur : ");
			ioe.printStackTrace();
		}
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == intensitePlus){
			if(this.fen.getPanelFigure().getFigure().getIntensiteLumineuse()<0.95)
				this.fen.getPanelFigure().getFigure().setIntensiteLumineuse(this.fen.getPanelFigure().getFigure().getIntensiteLumineuse() + 0.1);
			else
				this.fen.getPanelFigure().getFigure().setIntensiteLumineuse(1);

			this.fen.getPanelFigure().repaint();
		}
		if(e.getSource() == intensiteMoins){
			if(this.fen.getPanelFigure().getFigure().getIntensiteLumineuse()>0.15)
				this.fen.getPanelFigure().getFigure().setIntensiteLumineuse(this.fen.getPanelFigure().getFigure().getIntensiteLumineuse() - 0.1);
			else
				this.fen.getPanelFigure().getFigure().setIntensiteLumineuse(0);

			this.fen.getPanelFigure().repaint();
		}
		if(e.getSource() == open){
			new AssistantImport(fen);
		}
		if(e.getSource() == save){
			JFileChooser filechoose = new JFileChooser();
			int resultatEnregistrer;
			String approve;

			filechoose.setCurrentDirectory(new File(".")); /* ouvrir la boite de dialogue dans répertoire courant */
			filechoose.setDialogTitle("Enregistrer la figure"); /* nom de la boite de dialogue */
			filechoose.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY); /* pour afficher seulement les répertoires */

			approve = new String("Enregistrer"); /* Le bouton pour valider l’enregistrement portera la mention Enregistrer */

			resultatEnregistrer = filechoose.showDialog(filechoose, approve);

			if (resultatEnregistrer == JFileChooser.APPROVE_OPTION){ /* Si l’utilisateur clique sur le bouton Enregistrer */
				String chemin = filechoose.getSelectedFile().getPath()+"\\"; /* pour avoir le chemin absolu */
				if(this.fen.getPanelFigure().getFigure() != null)
					enregistrer_gts(chemin, this.fen.getPanelFigure().getNom());
				else{
					JOptionPane.showMessageDialog(null, "Il n'y a aucune figure d'ouverte");
				}
			}
		}
	}
}
