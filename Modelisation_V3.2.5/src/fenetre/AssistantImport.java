package fenetre;

import gestion_figure.Figure;
import gestion_figure.Panel_Figure;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import loading.LoadFigure;

import bdd.Connexion;

/**
 * La classe assistantimport gere la fenetre d'importation d'un nouveau fichier
 * @author remy
 *
 */

public class AssistantImport extends JFrame implements ActionListener{
	private JTextField cheminDeLaFigure = new JTextField(20);
	private JTextField nomDeLaFigure = new JTextField(20);
	private JTextField motCle = new JTextField(10);

	private JButton parcourir = new JButton("Parcourir");
	private JButton boutonEnregistrer = new JButton("Enregistrer");

	private Fenetre fen;

	private File fichier = null;

	private JPanel panelPrincipal = new JPanel();
	private JPanel panelParcourir = new JPanel();
	private JPanel panelTextCemin = new JPanel();
	private JPanel panelBoutonParcourir = new JPanel();
	private JPanel panelApercu = new JPanel();
	private JPanel panelNom = new JPanel();
	private JPanel panelMotCle = new JPanel();

	private Panel_Figure apercu;

	private LoadFigure loader = new LoadFigure();

	public AssistantImport(Fenetre fen) {
		this.fen = fen;
		this.apercu = new Panel_Figure();
		this.setTitle("Importation");
		this.setSize(400, 400);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(this.DISPOSE_ON_CLOSE);
		this.setVisible(true);
		
		this.parcourir.addActionListener(this);
		this.boutonEnregistrer.addActionListener(this);
		this.boutonEnregistrer.setEnabled(false);

		this.panelParcourir.setLayout(new BoxLayout(panelParcourir, BoxLayout.LINE_AXIS));
		this.panelTextCemin.add(cheminDeLaFigure);
		this.panelBoutonParcourir.add(parcourir);
		this.panelParcourir.add(panelTextCemin);
		this.panelParcourir.add(panelBoutonParcourir);
		this.apercu.setPreferredSize(new Dimension(100, 100));
		this.apercu.setBackground(Color.gray);
		this.panelApercu.add(new JLabel("Apercu : "));
		this.panelApercu.add(apercu);
		this.panelNom.add(new JLabel("Nom :"));
		this.panelNom.add(nomDeLaFigure);
		this.panelMotCle.add(new JLabel("Mot cle :"));
		this.panelMotCle.add(motCle);

		this.panelPrincipal.setLayout(new BoxLayout(panelPrincipal, BoxLayout.PAGE_AXIS));
		this.panelPrincipal.add(panelParcourir);
		this.panelPrincipal.add(panelApercu);
		this.panelPrincipal.add(panelNom);
		this.panelPrincipal.add(panelMotCle);
		this.panelPrincipal.add(boutonEnregistrer);
		this.setContentPane(panelPrincipal);
		this.pack();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		/**
		 * le bouton parcourir permet de selectionner le fichier de la figure(.gts)
		 */
		if(e.getSource() == parcourir){
			JFileChooser dialogue = new JFileChooser(new File("."));
			File proj = new File(System.getProperty("user.dir"));

			if (dialogue.showOpenDialog(null) == JFileChooser.APPROVE_OPTION){
				fichier = dialogue.getSelectedFile();
			}

			this.cheminDeLaFigure.setText("" + proj.toURI().relativize(fichier.toURI()));
			
			Figure fig = loader.creerFigure(fichier.getPath());
			
			if(fig != null){
				this.apercu.setFigure(fig, fichier.getPath(), nomDeLaFigure.getText());
				this.apercu.getFigure().setSegmentsEnable(false);
				this.apercu.getFigure().setLumiereEnable(true);
				this.apercu.getFigure().setFacesEnable(true);
				this.apercu.getFigure().setZoomRestreint(false);
				this.apercu.getFigure().setRotationAuto(false);
				this.apercu.getFigure().setRestreindreDeplacement(false);
				this.apercu.repaint();
				this.boutonEnregistrer.setEnabled(true);
			}
		}
		
		/**
		 * Le bouton enregistrer permet d'enregistrer la figure selectionne dans la base de donnee
		 */

		if(e.getSource() == boutonEnregistrer){
			boolean test = false;
			Scanner s = null;

			Connexion con = new Connexion("ProjetMode.db");
			con.connect();

			ResultSet rs = con.querySelect("select nom from figures");

			try {
				while (rs.next()) {
					if(rs.getString("nom") == fichier.getName().substring(0,fichier.getName().length() - 4)){
						test = true;
					}
				}
			} catch (SQLException e2) {
				e2.printStackTrace();
			} finally {			
				if(test == false){
					File proj = new File(System.getProperty("user.dir"));
					try {
						s = new Scanner(new FileReader(fichier.getPath()));
					} catch (FileNotFoundException e1) {
						e1.printStackTrace();
					}finally{
						con.query("insert into figures(nom, nbPoints, chemin) values ('" + nomDeLaFigure.getText() + "', '" + Integer.parseInt(s.next()) + "', '" + proj.toURI().relativize(fichier.toURI()) + "')");
						con.query("insert into motCle(mot) values ('" + nomDeLaFigure.getText() + "')");
						con.query("insert into association(figure, mot) values ('" + nomDeLaFigure.getText() + "', '" + motCle.getText() + "')");
						fen.getPanGauche().updateTree();
						fen.getPanGauche().revalidate();
					}
				}
				else{
					JOptionPane.showMessageDialog(null, "Il est d�j� pr�sent dans la base de donn�e !\nIl n'a donc pas �t� ajout� une deuxi�me fois");
				}
				con.close();
				this.dispose();
			}
		}
	}
}
