package fenetre;

import gestion_figure.Figure;
import gestion_figure.Panel_Figure;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;

import PanelDroit.Panel_droite;
import PanelGauche.Panel_gauche;

/**
 * La classe fenetre gere la fenetre dite "principale" Elle regroupe tout : la
 * figure, les boutons, ...
 *
 * @author remy
 *
 */

@SuppressWarnings("serial")
public class Fenetre extends JFrame {
	//Le panel de gauche est celui qui gere la base de donnee et la recherche
	private Panel_gauche panelGauche = new Panel_gauche(this);
	
	//Le panel de droite permet de gerer toutes les modifications sur une figure ainsi que les actions
	private Panel_droite panelDroite = new Panel_droite(this);
	
	//panel contenant la figure
	private Panel_Figure panelFigure = new Panel_Figure();

	//Barre d'outil
	private ToolBar barreOutil = new ToolBar(this);

	public Fenetre() {
		this.setTitle("Fenetre principale");
		this.setSize(new Dimension(1280, 720));
		this.setResizable(false);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setLayout(new BorderLayout());
		
		this.add(barreOutil, BorderLayout.NORTH);
		this.add(panelGauche, BorderLayout.WEST);
		this.add(panelDroite, BorderLayout.EAST);
		this.add(panelFigure, BorderLayout.CENTER);
		this.setVisible(true);
	}
	
	/**
	 * setPanelFigure permet d'ajouter une figure dans le panelFigure
	 * @param figure
	 * @param path
	 * @param nom
	 */
	
	public void setPanelFigure(Figure figure, String path, String nom){
		this.panelFigure.setFigure(figure, path, nom);
		
		this.getPanDroit().getOngletsPanelDroit().getPanEdition().getPanCouleurNom().getTextNom().setText("");
		this.getPanDroit().getOngletsPanelDroit().getPanEdition().getPanDescrip().ajoutText();
		this.getPanDroit().getOngletsPanelDroit().getPanEdition().getPanAffichage().setRadio();
		this.getPanDroit().getOngletsPanelDroit().getPanDeplacement().getPanAction().setRadio();
		
		this.panelDroite.getOngletsPanelDroit().getPanDeplacement().getPanBdr().setBouton();
		this.panelDroite.getOngletsPanelDroit().getPanDeplacement().getPanAction().setRadio();
		this.panelDroite.getOngletsPanelDroit().getPanEdition().getPanCouleurNom().setBouton();
		this.panelDroite.getOngletsPanelDroit().getPanEdition().setSauvegarde();
		
		this.barreOutil.setBouton();
		
		this.panelFigure.getFigure().setSegmentsEnable(false);
		this.panelFigure.getFigure().setLumiereEnable(true);
		this.panelFigure.getFigure().setFacesEnable(true);
		this.panelFigure.getFigure().setZoomRestreint(false);
		this.panelFigure.getFigure().setRotationAuto(false);
		this.panelFigure.getFigure().setRestreindreDeplacement(false);
		this.panelFigure.repaint();
		
		this.revalidate();
	}
	
	public ToolBar getBarreOutil() {
		return barreOutil;
	}
	
	public Panel_Figure getPanelFigure(){
		return this.panelFigure;
	}
	
	public Panel_gauche getPanGauche(){
		return panelGauche;
	}
	
	public Panel_droite getPanDroit(){
		return panelDroite;
	}
}
