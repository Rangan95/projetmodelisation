package bdd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

/**
 * La classe connexion permet de se connecter a la base de donnee
 * @author remy
 *
 */

public class Connexion {
	private String dbPath;
	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;
	
	public Connexion(String dbPath) {
		this.dbPath = dbPath;
	}
	
	/**
	 * querySelect permet d'executer une requete qui retourne un resultset(c'est un select)
	 * @param requete
	 * @return
	 */

	public ResultSet querySelect(String requete) {		
		try {
			rs = stmt.executeQuery(requete);
		}catch(SQLException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Erreur dans la requete : " + requete);
		}
		
		return rs;
	}
	
	/**
	 * query permetd'executer toutes les requetes mise a part le select car lui demande un resultset en retour
	 * @param requete
	 */
	
	public void query(String requete){
		try {
			stmt.executeUpdate(requete);
		}catch(SQLException e) {
			JOptionPane.showMessageDialog(null, "Deja present dans la base de donnee");
		}
	}
	
	/**
	 * connect permet de se connecter a la base de donnee
	 */

	public void connect() {
		try {
			Class.forName("org.sqlite.JDBC");
			con = DriverManager.getConnection("jdbc:sqlite:" + dbPath);

			stmt = con.createStatement();
			System.out.println("Connexion a " + dbPath);
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Erreur : Impossible de se connecter a la base de donnee");
		}catch(SQLException e){
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Erreur : Impossible de se connecter a la base de donnee");
		}
	}
	
	/**
	 * close permet de fermer la connection avec la base de donnee
	 */

	public void close() {
		try {
			con.close();
			System.out.println("Fermeture de la connexion");
		}catch(SQLException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Erreur : Fermeture de la base de donnee");
		}
	}
}
