package PanelEdition;

import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fenetre.Fenetre;

/**
 * Cette classe permet de gerer lajout de mot(l'ihm)
 * @author remy
 *
 */

public class Panel_MotCle extends JPanel {
	private JTextField text;
	private Fenetre fen;
	private Box box = Box.createVerticalBox();
	private JLabel lab = new JLabel("Mot cle : ");
	
	public Panel_MotCle(Fenetre fen){
		this.fen = fen;
		this.text = new JTextField();
		
		this.setPreferredSize(new Dimension(200, 50));
		this.text.setPreferredSize(new Dimension(200, 30));
		
		this.box.add(lab);
		this.box.add(text);
		this.add(box);
	}
	
	public JTextField getTextField(){
		return this.text;
	}
}
