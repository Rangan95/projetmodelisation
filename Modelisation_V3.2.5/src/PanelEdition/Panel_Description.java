package PanelEdition;

import java.awt.Dimension;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import bdd.Connexion;

import fenetre.Fenetre;

/**
 * Cette classe permet de gerer le panel de description de la figure
 * @author remy
 *
 */

public class Panel_Description extends JPanel {
	private JTextField text;
	private Fenetre fen;
	private Box box = Box.createVerticalBox();
	private JLabel lab = new JLabel("Description :");
	
	public Panel_Description(Fenetre fen){
		this.fen = fen;
		this.text = new JTextField();
		
		this.setPreferredSize(new Dimension(200, 150));
		this.text.setPreferredSize(new Dimension(200, 130));
		
		this.box.add(lab);
		this.box.add(text);
		this.add(box);
	}
	
	public JTextField getTextField(){
		return this.text;
	}
	
	/**
	 * Cette fonction permet de mofifier le texte present dans le textField
	 */
	
	public void ajoutText(){
		Connexion con = new Connexion("ProjetMode.db");
		con.connect();
		ResultSet rs = con.querySelect("Select Description from figures where chemin = '" + this.fen.getPanelFigure().getPath() + "'");
		try {
			if(rs.getString("Description") == null)
				this.text.setText("");
			else
				this.text.setText("" + rs.getString("Description"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			con.close();
		}
	}
}
