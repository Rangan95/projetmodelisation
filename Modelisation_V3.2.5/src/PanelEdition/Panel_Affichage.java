package PanelEdition;

import fenetre.Fenetre;

import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Cette classe permet de gerer le panel qui permet de parametrer comment la figure s'affiche
 * @author remy
 *
 */

public class Panel_Affichage extends JPanel implements ItemListener {
	private JLabel lab_segments = new JLabel("Segments");
	private JLabel lab_faces = new JLabel("Faces");
	private JLabel lab_lumieres = new JLabel("Lumiere");
	private JLabel lab_rotationSensi = new JLabel("Rotation Sensibilité : ");
	private JLabel lab_zoomSensi = new JLabel("Translation Sensibilite : ");

	private JCheckBox check_segments = new JCheckBox();
	private JCheckBox check_faces = new JCheckBox();
	private JCheckBox check_lumieres = new JCheckBox();

	private Box boxAffichage = Box.createHorizontalBox();
	private Box boxPrincipale = Box.createVerticalBox();

	private Fenetre fen;

	public Panel_Affichage(Fenetre fen){
		this.fen = fen;
		this.setPreferredSize(new Dimension(280, 80));
		
		this.check_faces.setEnabled(false);
		this.check_lumieres.setEnabled(false);
		this.check_segments.setEnabled(false);
		
		this.check_faces.setSelected(true);
		this.check_lumieres.setSelected(true);

		this.check_segments.addItemListener(this);
		this.check_faces.addItemListener(this);
		this.check_lumieres.addItemListener(this);

		this.boxAffichage.add(check_segments);
		this.boxAffichage.add(lab_segments);
		this.boxAffichage.add(check_faces);
		this.boxAffichage.add(lab_faces);
		this.boxAffichage.add(check_lumieres);
		this.boxAffichage.add(lab_lumieres);

		this.boxPrincipale.add(boxAffichage);

		this.add(boxPrincipale);
	}
	
	/**
	 * cette fonction permet de mettre les boutons dans leur etat par defaut
	 */
	
	public void setRadio(){
		this.check_faces.setEnabled(true);
		this.check_lumieres.setEnabled(true);
		this.check_segments.setEnabled(true);
		this.check_faces.setSelected(true);
		this.check_lumieres.setSelected(true);
		this.check_segments.setSelected(false);
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		if(e.getSource() == check_segments){
			this.fen.getPanelFigure().getFigure().setSegmentsEnable(!this.fen.getPanelFigure().getFigure().isSegmentsEnable());
		}
		if(e.getSource() == check_faces){
			this.fen.getPanelFigure().getFigure().setFacesEnable(!this.fen.getPanelFigure().getFigure().isFacesEnable());
		}
		if(e.getSource() == check_lumieres){
			this.fen.getPanelFigure().getFigure().setLumiereEnable(!this.fen.getPanelFigure().getFigure().isLumiereEnable());
		}
		this.fen.getPanelFigure().repaint();
	}
}
