package PanelEdition;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JPanel;

import gestion_figure.Panel_Figure;

/**
 * Cette classe permet de gerer la fenetre qui permet de changer la couleur
 * @author remy
 *
 */

public class PanelCouleur extends JDialog implements ActionListener{
	private JColorChooser colorChooser = new JColorChooser();
	private JPanel p1 = new JPanel();
	private JButton ok = new JButton("Ok");
	private Panel_Figure panelFigure;
	private int val;
	
	public PanelCouleur(Panel_Figure panelFigure, int val){
		this.panelFigure = panelFigure;
		this.val = val;
		this.setTitle("Nouvelle couleur pour la figure");
		this.setSize(new Dimension(650,400));
		this.setLocationRelativeTo(null);
		this.setAlwaysOnTop(true);
		this.setLayout(new BorderLayout());
		this.ok.addActionListener(this);
		this.add(colorChooser,BorderLayout.CENTER);
		this.p1.add(ok);
		this.add(p1,BorderLayout.SOUTH);
		this.setModal(true);
		this.setDefaultCloseOperation(this.DISPOSE_ON_CLOSE);
		this.setVisible(true);
		this.pack();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == ok && val == 0){
			this.panelFigure.getFigure().setColor(colorChooser.getColor());
			this.panelFigure.repaint();
			this.dispose();
		}
		if(e.getSource() == ok && val == 1){
			this.panelFigure.setColor((colorChooser.getColor()));
			this.panelFigure.repaint();
			this.dispose();
		}
	}
}
