package PanelEdition;

import fenetre.Fenetre;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * Cette fonction permet de gerer le panel qui permet de modifier les couleurs ou le nom de la figure
 * @author remy
 *
 */

public class Panel_CouleurNom extends JPanel implements ActionListener {
	private JLabel lab_couleurFond = new JLabel("Couleur de fond : ");
	private JLabel lab_couleurFigure = new JLabel("Couleur de la figure : ");
	private JLabel lab_nom = new JLabel("Nom de la figure : ");
	
	private JButton boutonColorFigure = new JButton();
	private JButton boutonFondEcran = new JButton();
	
	private JTextField text_nom = new JTextField();
	
	private Box boxCouleurFond = Box.createHorizontalBox();
	private Box boxCouleurFigure = Box.createHorizontalBox();
	private Box boxNomFigure = Box.createVerticalBox();
	private Box boxPrincipale = Box.createVerticalBox();
	
	private Fenetre fen;
	
	public Panel_CouleurNom(Fenetre fen){
		this.fen = fen;
		
		this.setPreferredSize(new Dimension(280, 230));
		
		this.boutonColorFigure.setEnabled(false);
		this.boutonFondEcran.setEnabled(false);
		
		this.initButton("../Icones/boutonColor.png", boutonColorFigure);
		this.initButton("../Icones/boutonFondEcran.png", boutonFondEcran);
		
		this.boxCouleurFond.add(lab_couleurFond);
		this.boxCouleurFond.add(boutonFondEcran);
		
		this.boxCouleurFigure.add(lab_couleurFigure);
		this.boxCouleurFigure.add(boutonColorFigure);
		
		this.boxNomFigure.add(lab_nom);
		this.boxNomFigure.add(text_nom);
		
		this.boxPrincipale.add(Box.createRigidArea(new Dimension(0, 60)));
		this.boxPrincipale.add(boxCouleurFond);
		this.boxPrincipale.add(Box.createRigidArea(new Dimension(0, 30)));
		this.boxPrincipale.add(boxCouleurFigure);
		this.boxPrincipale.add(Box.createRigidArea(new Dimension(0, 30)));
		this.boxPrincipale.add(boxNomFigure);
		
		this.add(boxPrincipale);
	}
	
	/**
	 * Cette fonction permet de mettre les boutons cliquable
	 */
	
	public void setBouton(){
		this.boutonColorFigure.setEnabled(true);
		this.boutonFondEcran.setEnabled(true);
	}
	
	/**
	 * Cette fonction permet de changer le design des boutons
	 * @param cheminImage
	 * @param bouton
	 */
	
	private void initButton(String cheminImage, JButton bouton){
		bouton.setIcon(new ImageIcon(cheminImage));
		bouton.setBorderPainted(false);
		bouton.addActionListener(this);
	}
	
	public JTextField getTextField(){
		return text_nom;
	}
	
	public JTextField getTextNom(){
		return text_nom;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == boutonColorFigure){
			new PanelCouleur(this.fen.getPanelFigure(), 0);
		}
		if(e.getSource() == boutonFondEcran){
			new PanelCouleur(this.fen.getPanelFigure(), 1);
		}
	}
}
