package PanelEdition;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import bdd.Connexion;

import fenetre.Fenetre;

/**
 * Cette classe permet de gerer le panel d'edition. Ce panel permet de changer les infos d'une figure
 * @author remy
 *
 */

public class Panel_Edition extends JPanel implements ActionListener{
	private Fenetre fen;
	private JButton boutonSauvegarde = new JButton("Sauvegarde de la figure");
	private Box boxPrincipale = Box.createVerticalBox();
	private Box boxBouton = Box.createHorizontalBox();
	private Panel_CouleurNom pan_haut;
	private Panel_Affichage pan_bas;
	private Panel_Description pan_description;
	private Panel_MotCle pan_motCle;

	public Panel_Edition(Fenetre fen){
		this.fen = fen;		
		this.setPreferredSize(new Dimension(280, 600));

		this.boutonSauvegarde.setEnabled(false);
		this.boutonSauvegarde.addActionListener(this);

		this.pan_haut = new Panel_CouleurNom(fen);
		this.pan_bas = new Panel_Affichage(fen);
		this.pan_description = new Panel_Description(fen);
		this.pan_motCle = new Panel_MotCle(fen);
		this.boxBouton.add(boutonSauvegarde);
		this.boxPrincipale.add(pan_haut);
		this.boxPrincipale.add(pan_bas);
		this.boxPrincipale.add(pan_description);
		this.boxPrincipale.add(pan_motCle);
		this.boxPrincipale.add(Box.createRigidArea(new Dimension(0, 30)));
		this.boxPrincipale.add(boxBouton);

		this.add(boxPrincipale);
	}
	
	public void setSauvegarde(){
		this.boutonSauvegarde.setEnabled(true);
	}

	public Panel_CouleurNom getPanCouleurNom(){
		return pan_haut;
	}

	public Panel_Description getPanDescrip(){
		return this.pan_description;
	}

	public Panel_Affichage getPanAffichage(){
		return this.pan_bas;
	}

	@Override
	public void actionPerformed(ActionEvent e){
		if(e.getSource() == boutonSauvegarde){
			if(this.fen.getPanelFigure().getFigure() == null){
				JOptionPane.showMessageDialog(null, "Il n'y a aucune figure d'ouverte");
			}
			else{
				Connexion con = new Connexion("ProjetMode.db");
				con.connect();

				ResultSet rs = con.querySelect("select nom from figures");
				if(this.fen.getPanelFigure() != null){
					if(!this.pan_haut.getTextField().getText().isEmpty())
						con.query("update figures set nom = '" + pan_haut.getTextField().getText() + "' where chemin = '" + this.fen.getPanelFigure().getPath() + "'");
					con.query("update figures set Description = '" + pan_description.getTextField().getText() + "'where chemin = '" + this.fen.getPanelFigure().getPath() + "'");
					if(!pan_motCle.getTextField().getText().isEmpty()){
						con.query("insert into motCle(mot) values ('" + pan_motCle.getTextField().getText() + "')");
						con.query("insert into association(figure, mot) values ('" + this.fen.getPanelFigure().getNom() + "', '" + pan_motCle.getTextField().getText() + "')");
					}
					this.fen.getPanGauche().updateTree();
					this.fen.getPanGauche().revalidate();
				}
				else{
					JOptionPane.showMessageDialog(null, "Une figure porte deja ce nom");
				}
				con.close();
			}
		}
	}
}
