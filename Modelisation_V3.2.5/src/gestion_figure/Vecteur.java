package gestion_figure;

/**
 * Permet de modeliser un vecteur dont on a besoin pour realiser la lumiere
 * 
 * @author remy
 *
 */

public class Vecteur {
	private double x;
	private double y;
	private double z;

	public Vecteur() {
	}

	public Vecteur(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Vecteur(Point pointA, Point pointB) {
		x = pointB.getX() - pointA.getX();
		y = pointB.getY() - pointA.getY();
		z = pointB.getZ() - pointA.getZ();
	}

	/**
	 * Permet de realiser un produit vectoriel
	 * 
	 * @param vecteur2
	 * @return
	 */

	public Vecteur produitVectoriel(Vecteur vecteur2) {
		double xtmp, ytmp, ztmp;

		xtmp = y * vecteur2.getZ() - z * vecteur2.getY();
		ytmp = -x * vecteur2.getZ() + z * vecteur2.getX();
		ztmp = x * vecteur2.getY() - y * vecteur2.getX();

		return new Vecteur(xtmp, ytmp, ztmp);
	}

	/**
	 * Permet de realiser un produit scalaire
	 * 
	 * @param vecteur2
	 * @return
	 */

	public double produitScalaire(Vecteur vecteur2) {
		return x * vecteur2.getX() + y * vecteur2.getY() + z * vecteur2.getZ();
	}

	public double getNorme() {
		return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getZ() {
		return z;
	}
}
