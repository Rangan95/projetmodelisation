package gestion_figure;

/**
 * Cette classe permet de modeliser un point de la figure
 * @author remy
 *
 */

public class Point{
	private double x, y, z;
	
	public Point(double x, double y, double z){
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	/**
	 * Nous avons besoin de cette methode pour pouvoir effectuer le zoom
	 * @param i
	 */
	
	protected void zoom(double i){
		x = x*i;
		y = y*i;
		z = z*i;
	}
	
	public void setX(double x){
		this.x = x;
	}
	
	public double getX(){
		return x;
	}
	
	public void setY(double y){
		this.y = y;
	}
	
	public double getY(){
		return y;
	}
	
	public void setZ(double z){
		this.z = z;
	}
	
	public double getZ(){
		return z;
	}
	
	public String toString(){
		return "x = " + x + ", y = " + y + ", z = " + z;
	}
}
