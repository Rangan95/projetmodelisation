package gestion_figure;

/**
 * Permet de modeliser un segment de la figure
 * @author remy
 *
 */

public class Segment{
	private Point pointA, pointB;
	
	public Segment(Point pointA, Point pointB){
		this.pointA = pointA;
		this.pointB = pointB;
	}
	
	public void setPointA(Point a){
		this.pointA = a;
	}
	
	public Point getPointA(){
		return pointA;
	}
	
	public void setPointB(Point b){
		this.pointB = b;
	}
	
	public Point getPointB(){
		return pointB;
	}
}
