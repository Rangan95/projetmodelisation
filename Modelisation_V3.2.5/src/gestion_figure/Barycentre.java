package gestion_figure;

public class Barycentre extends Point {

	public Barycentre(double x, double y, double z) {
		super(x, y, z);
	}

	/**
	 * Methode qui calcul le barycentre dun objet operable(ici une figure)
	 * Pour chaque point, on ajoute dans le tableau correspondant les coordonnees x, y et z du point
	 */

	public static void calculBarycentre(Operable f) {
		double sumX = 0, sumY = 0, sumZ = 0, d;

		for (Point p : f.getPoint()) {
			sumX += p.getX();
			sumY += p.getY();
			sumZ += p.getZ();
		}

		d = f.getPoint().length;
		f.getBarycentreFigure().setBarycentre(new Point(sumX / d, sumY / d, sumZ / d));
	}

	/**
	 * Methode qui permet de calculer le barycentre d'une face. Elle fonctionne
	 * de la meme maniere que celle pour la figure mais avec moins de point
	 */

	public static void calculBarycentre(Face face) {
		double x = (face.getP1().getX() + face.getP2().getX() + face.getP3().getX()) / 3;
		double y = (face.getP1().getY() + face.getP2().getY() + face.getP3().getY()) / 3;
		double z = (face.getP1().getZ() + face.getP2().getZ() + face.getP3().getZ()) / 3;

		face.getBarycentreFace().setBarycentre(new Point(x, y, z));
	}

	/**
	 * Cette fonction permet de mettre a jour le barycentre
	 * @param Nbarycentre
	 */

	private void setBarycentre(Point Nbarycentre) {
		setX(Nbarycentre.getX());
		setY(Nbarycentre.getY());
		setZ(Nbarycentre.getZ());
	}

	/**
	 * Cette fontion permet de definir l'affichage du barycentre
	 * @return barycentre
	 */

	@Override
	public String toString() {
		return "x: " + getX() + "\ny: " + getY();
	}
}
