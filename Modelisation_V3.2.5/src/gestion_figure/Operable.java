package gestion_figure;

import java.util.Arrays;

/**
 * La classe operable permet d'effectuer toutes les modifications que l'on veut
 * faire sur une figure
 *
 * @author remy
 *
 */

public class Operable {
	protected Point[] points;
	protected Face[] faces;
	protected Barycentre barycentreFigure;
	private int vZoom;

	public Operable(Point[] points, Face[] faces) {
		this.points = points;
		this.faces = faces;

		barycentreFigure = new Barycentre(0, 0, 0);
		Barycentre.calculBarycentre(this);

		vZoom = 0;
	}

	/**
	 * Cette fonction tri toutes les faces d'une figure par rapport l'axe des z
	 *
	 */

	public void algodupeintre() {
		Arrays.sort(faces);
	}

	/**
	 * Cette fonction effectue un zoom jusqu'a un maximum de 20 fois
	 *
	 * @param i
	 */

	public void zoom(double i) {
		double transX = barycentreFigure.getX();
		double transY = barycentreFigure.getY();

		translation(-transX, -transY);

		if (i < 1) {
			if (vZoom > -20) {
				for (Point p : points) {
					p.zoom(i);
				}
				vZoom--;
			}
		} else if (i > 1) {
			if (vZoom < 20) {
				for (Point p : points) {
					p.zoom(i);
				}
				vZoom++;
			}
		}

		translation(transX, transY);
	}

	/**
	 * Permet une rotation par rapport a l'axe des Y
	 *
	 * @param i
	 */

	public void rotationY(double i) {
		Barycentre.calculBarycentre(this);
		double transX = barycentreFigure.getX();
		double transY = barycentreFigure.getY();
		double tmpx, tmpz;

		for (Point p : points) {
			tmpx = p.getX();
			tmpz = p.getZ();

			p.setX(Math.cos(i) * tmpx - Math.sin(i) * tmpz);
			p.setZ(Math.sin(i) * tmpx + Math.cos(i) * tmpz);
		}

		Barycentre.calculBarycentre(this);
		translation(transX - barycentreFigure.getX(), transY - barycentreFigure.getY());

		algodupeintre();
	}

	/**
	 * permet une rotation par rapport a l'axe des X
	 *
	 * @param i
	 */

	public void rotationX(double i) {
		Barycentre.calculBarycentre(this);
		double transX = barycentreFigure.getX();
		double transY = barycentreFigure.getY();
		double tmpy, tmpz;

		for (Point p : points) {
			tmpy = p.getY();
			tmpz = p.getZ();

			p.setY(Math.cos(i) * tmpy - Math.sin(i) * tmpz);
			p.setZ(Math.sin(i) * tmpy + Math.cos(i) * tmpz);
		}

		Barycentre.calculBarycentre(this);
		translation(transX - barycentreFigure.getX(), transY - barycentreFigure.getY());

		algodupeintre();
	}

	/**
	 * Permet d'effectuer une translation
	 *
	 * @param i
	 * @param j
	 */

	public void translation(double i, double j) {
		for (Point p : points) {
			p.setX(p.getX() + i);
			p.setY(p.getY() + j);
		}

		for (Face f : this.faces) {
			f.getBarycentreFace().setX(f.getBarycentreFace().getX() + i);
			f.getBarycentreFace().setY(f.getBarycentreFace().getY() + j);
		}

		Barycentre.calculBarycentre(this);
	}

	/**
	 * Permet de de changer la variable vZoom.
	 *
	 * @param vZoom
	 */

	public void setZoom(int vZoom) {
		this.vZoom = vZoom;
	}

	public Point[] getPoint() {
		return points;
	}

	public Face[] getFace() {
		return faces;
	}

	public Barycentre getBarycentreFigure() {
		return barycentreFigure;
	}
}
