package gestion_figure;

import java.awt.Color;
import java.awt.Graphics;

/**
 * La classe figure represente une figure du fichier gts. Elle encapsule donc :
 * Point, Segement et Face pour representer la figure. Elle etend Operable
 * puisque la figure est un objet sur lequelle on peut effectuer des calculs
 *
 * @author remy
 */

public class Figure extends Operable {
	private Segment[] segments;
	private Color couleur = new Color(238, 209, 83);
	
	private boolean segmentsEnable;
	private boolean facesEnable;
	private boolean pointsEnable;
	private boolean lumiereEnable;
	private double intensiteLumineuse;
	private boolean restreindreDeplacement;
	private boolean zoomRestreint;
	private boolean rotationEnable;
	private double rotationSensi;
	private double transSensi;

	public Figure(Point[] points, Segment[] segments, Face[] faces) {
		super(points, faces);
		this.segments = segments;
		this.pointsEnable = false;
		this.intensiteLumineuse = 1;
		this.rotationSensi = 0.1;
		this.transSensi = 3;
	}

	/**
	 * Permet d'affichier la figure
	 *
	 * @param g
	 */

	public void affichage(Graphics g) {
		double intensite = 1 * intensiteLumineuse;

		for (Face f : faces) {
			Barycentre.calculBarycentre(f);
		}

		this.algodupeintre();

		for (Face f : faces) {
			if (lumiereEnable) {
				intensite = f.getIntensiteLumineuse()*intensiteLumineuse;;
			}
			
			g.setColor(new Color((int) (couleur.getRed() * intensite),
					(int) (couleur.getGreen() * intensite), (int) (couleur.getBlue() * intensite)));

			if (facesEnable) {
				g.fillPolygon(f.getFace());
			}

			if (segmentsEnable) {
				g.setColor(Color.BLACK);
				g.drawLine((int) f.getS1().getPointA().getX(), (int) f.getS1().getPointA().getY(),
						(int) f.getS1().getPointB().getX(), (int) f.getS1().getPointB().getY());

				g.drawLine((int) f.getS2().getPointA().getX(), (int) f.getS2().getPointA().getY(),
						(int) f.getS2().getPointB().getX(), (int) f.getS2().getPointB().getY());

				g.drawLine((int) f.getS3().getPointA().getX(), (int) f.getS3().getPointA().getY(),
						(int) f.getS3().getPointB().getX(), (int) f.getS3().getPointB().getY());
			}
			if (pointsEnable){
				g.setColor(Color.WHITE);
				g.fillOval((int)f.getP1().getX()-1, (int)f.getP1().getY()-1, 2, 2);
				g.fillOval((int)f.getP2().getX()-1, (int)f.getP2().getY()-1, 2, 2);
				g.fillOval((int)f.getP3().getX()-1, (int)f.getP3().getY()-1, 2, 2);
			}
		}
	}

	/**
	 * Cette fonction permet de trouver la coordonnee maximum en Y
	 *
	 * @return min
	 */

	public Point calculMaxY() {
		Point min = null;
		Point tmp;

		for (Point p : this.getPoint()) {
			tmp = p;

			if (min == null) {
				min = tmp;
			} else if (tmp.getY() > min.getY()) {
				min = tmp;
			}
		}

		return min;
	}

	/**
	 * Cette fonction permet de trouver la coordonnee maximum en x
	 *
	 * @return min
	 */

	public Point calculMaxX() {
		Point min = null;
		Point tmp;
		
		for (Point p : points) {
			tmp = p;
			if (min == null) {
				min = tmp;
			} else if (tmp.getX() > min.getX()) {
				min = tmp;
			}
		}

		return min;
	}

	/**
	 * Cette fonction permet de trouver la coordonnee minimum en Y
	 *
	 * @return min
	 */

	public Point calculMinY() {
		Point min = null;
		Point tmp;
		
		for (Point p : points) {
			tmp = p;
			if (min == null) {
				min = tmp;
			} else if (tmp.getY() < min.getY()) {
				min = tmp;
			}
		}

		return min;
	}

	/**
	 * Cette fonction permet de trouver la coordonnee minimum en x
	 *
	 * @return min
	 */

	public Point calculMinX() {
		Point min = null;
		Point tmp;
		
		for (Point p : points) {
			tmp = p;
			if (min == null) {
				min = tmp;
			} else if (tmp.getX() < min.getX()) {
				min = tmp;
			}
		}

		return min;
	}

	public Segment[] getSegment() {
		return segments;
	}
	
	public void setColor(Color couleur){
		this.couleur = couleur;
	}

	public boolean isSegmentsEnable() {
		return  this.segmentsEnable;
	}

	public void setSegmentsEnable(boolean segmentsEnable) {
		this.segmentsEnable = segmentsEnable;
	}

	public boolean isFacesEnable() {
		return  this.facesEnable;
	}

	public void setFacesEnable(boolean facesEnable) {
		this.facesEnable = facesEnable;
	}

	public boolean isPointsEnable() {
		return  this.pointsEnable;
	}

	public void setPointsEnable(boolean pointsEnable) {
		this.pointsEnable = pointsEnable;
	}

	public boolean isLumiereEnable() {
		return  this.lumiereEnable;
	}

	public void setLumiereEnable(boolean lumiereEnable) {
		this.lumiereEnable = lumiereEnable;
	}

	public double getIntensiteLumineuse() {
		return  this.intensiteLumineuse;
	}

	public void setIntensiteLumineuse(double intensiteLumineuse) {
		this.intensiteLumineuse = intensiteLumineuse;
	}

	public boolean isRestreindreDeplacement() {
		return  this.restreindreDeplacement;
	}

	public void setRestreindreDeplacement(boolean restreindreDeplacement) {
		this.restreindreDeplacement = restreindreDeplacement;
	}

	public boolean isZoomRestreint() {
		return  this.zoomRestreint;
	}

	public void setZoomRestreint(boolean zoomRestreint) {
		this.zoomRestreint = zoomRestreint;
	}

	public double getRotationSensi() {
		return  this.rotationSensi;
	}

	public void setRotationSensi(double rotationSensi) {
		this.rotationSensi = rotationSensi;
	}

	public double getTransSensi() {
		return transSensi;
	}

	public void setTransSensi(double transSensi) {
		this.transSensi = transSensi;
	}
	
	public boolean isRotationAuto() {
		return rotationEnable;
	}

	public void setRotationAuto(boolean rotationEnable) {
		this.rotationEnable = rotationEnable;
	}
}
