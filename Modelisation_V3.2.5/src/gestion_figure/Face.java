package gestion_figure;

import java.awt.Polygon;

/**
 * La classe Face representre une face de la figure qui est donc compose de 3 segments(c'est un triangle). 
 * Pour l'algo du peintre, on a besoin d'implementer Comparable
 *
 * @author remy
 */

public class Face implements Comparable<Face> {
	private Segment s1, s2, s3;
	private Point p1, p2, p3;
	private Barycentre barycentreFace;

	public Face(Segment s1, Segment s2, Segment s3) {
		this.s1 = s1;
		this.s2 = s2;
		this.s3 = s3;

		p1 = s1.getPointA();
		p2 = s1.getPointB();

		if (!getS1().getPointA().equals(getS2().getPointA()) && !getS1().getPointB().equals(getS2().getPointA())) {
			p3 = s2.getPointA();
		} else {
			p3 = s2.getPointB();
		}

		barycentreFace = new Barycentre(0, 0, 0);
		Barycentre.calculBarycentre(this);
	}

	/**
	 * Cette fonction permet de gerer la lumiere
	 *
	 * @return resultat
	 */

	public double getIntensiteLumineuse() {
		Vecteur normal = new Vecteur();
		Vecteur u1 = new Vecteur(s1.getPointA(), s1.getPointB());
		Vecteur u2 = new Vecteur(s2.getPointA(), s2.getPointB());
		Vecteur sourceLumineuse = new Vecteur(0, 0, 1);
		double resultat;

		normal = u1.produitVectoriel(u2);

		// calcul de l'intensite lumineuse
		resultat = Math.abs(normal.produitScalaire(sourceLumineuse)) / (sourceLumineuse.getNorme() * normal.getNorme());

		return resultat;
	}

	public void setS1(Segment s1) {
		this.s1 = s1;
	}

	public Segment getS1() {
		return s1;
	}

	public void setS2(Segment s2) {
		this.s2 = s2;
	}

	public Segment getS2() {
		return s2;
	}

	public void setS3(Segment s3) {
		this.s3 = s3;
	}

	public Segment getS3() {
		return s3;
	}

	public Point getP1() {
		return p1;
	}

	public Point getP2() {
		return p2;
	}

	public Point getP3() {
		return p3;
	}

	public Polygon getFace() {
		int[] coordX = new int[] { (int) p1.getX(), (int) p2.getX(), (int) p3.getX() };
		int[] coordY = new int[] { (int) p1.getY(), (int) p2.getY(), (int) p3.getY() };

		return new Polygon(coordX, coordY, 3);
	}

	public Barycentre getBarycentreFace() {
		return barycentreFace;
	}

	/**
	 * Permet de comparer le barycentre a la coordonnee z de deux faces
	 */

	@Override
	public int compareTo(Face f) {
		return (int) barycentreFace.getZ() - (int) f.getBarycentreFace().getZ();
	}
}
