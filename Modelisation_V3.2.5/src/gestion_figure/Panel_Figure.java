package gestion_figure;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;

import Listener.ListenerPanel_Figure;

/**
 * Cette classe etend JPanel qui contient la figure
 *
 * @author remy
 * */

public class Panel_Figure extends JPanel {
	private Figure figure;
	private String  path;
	private String nom;
	private Color couleur = Color.GRAY;
	
	public Panel_Figure() {
		this.setSize(new Dimension(772, 592));
	}

	public void zoomParDefaut() {
		this.figure.zoom(20.0);
	}

	/**
	 * Cette methode permet de mettre le barycentre de la figure a la coordonnee 0,0
	 */

	public void remiseCoordZero() {
		for (Point p : figure.getPoint()) {
			p.setX(p.getX() - this.figure.getBarycentreFigure().getX());
			p.setY(p.getY() - this.figure.getBarycentreFigure().getY());
		}
	}

	/**
	 * Affiche la figure On fait un fillrect pour effacer la figure a chaque
	 * repaint
	 */

	@Override
	public void paintComponent(Graphics g) {
		g.setColor(couleur);
		g.fillRect(0, 0, getWidth(), getHeight());

		if (figure != null) {
			figure.affichage(g);
		}
	}
	
	/**
	 * Permet d'initialiser la figure dans le panel avec des "relgles" par
	 * defaut
	 *
	 * @param figure
	 */

	public void setFigure(Figure figure, String path, String nom) {
		this.figure = figure;
		this.path = path;
		this.nom = nom;
		
		this.couleur = Color.GRAY;
		
		ListenerPanel_Figure listener = new ListenerPanel_Figure(this);
		this.addMouseWheelListener(listener);
		this.addMouseListener(listener);
		this.addMouseMotionListener(listener);

		this.remiseCoordZero();
		this.figure.translation(getSize().getWidth() / 2, getSize().getHeight() / 2);

		while (this.figure.calculMaxX().getX() < this.getSize().getWidth() - 20
				&& this.figure.calculMaxY().getY() < this.getSize().getHeight() - 20
				&& this.figure.calculMinX().getX() > 20
				&& this.figure.calculMinY().getY() > 20) {
			this.figure.zoom(1.1);
			this.figure.setZoom(0);
		}
	}

	public Figure getFigure() {
		return this.figure;
	}
	
	public void setColor(Color newCouleur){
		this.couleur = newCouleur;
	}
	
	public String getPath(){
		return this.path;
	}
	
	public String getNom(){
		return this.nom;
	}
}
