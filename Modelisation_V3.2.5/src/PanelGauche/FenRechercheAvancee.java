package PanelGauche;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.tree.DefaultMutableTreeNode;

import fenetre.Fenetre;

import bdd.Connexion;

/**
 * cette classe permet de gerer ma fenetre de recherche avancee
 * @author remy
 *
 */

public class FenRechercheAvancee extends JFrame implements ActionListener {
	private JPanel pan = new JPanel();
	private JButton rechercher = new JButton("Rechercher");
	private JLabel lab_motCle = new JLabel("Mot Clé : ");
	private JTextField text_motCle = new JTextField();
	private JLabel lab_nbPoints = new JLabel("Nombre de points : ");
	private JTextField text_nbPoints = new JTextField();
	private ArrayList<DefaultMutableTreeNode> list = new ArrayList<DefaultMutableTreeNode>();
	private Panel_gauche pg;
	private Fenetre fen;
	
	private Box boxPrincipale = Box.createVerticalBox();
	private Box boxMotCle = Box.createHorizontalBox();
	private Box boxNbPoints = Box.createHorizontalBox();
	
	public FenRechercheAvancee(Fenetre fen, Panel_gauche pg){
		this.pg = pg;
		this.fen = fen;
		this.setPreferredSize(new Dimension(500, 170));
		this.setVisible(true);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setTitle("Recherche Avancee");
		this.setContentPane(buildPanel());
		this.setVisible(true);
		this.pack();
	}
	
	/**
	 * Cette fonction permet de definir le panel de la fenetre
	 * @return
	 */
	
	public JPanel buildPanel(){		
		this.rechercher.addActionListener(this);
		
		this.boxMotCle.add(lab_motCle);
		this.boxMotCle.add(text_motCle);
		this.boxNbPoints.add(lab_nbPoints);
		this.boxNbPoints.add(text_nbPoints);
		this.boxPrincipale.add(boxMotCle);
		this.boxPrincipale.add(boxNbPoints);
		this.boxPrincipale.add(Box.createRigidArea(new Dimension(0, 50)));
		this.boxPrincipale.add(rechercher);
		
		this.pan.add(boxPrincipale);
		
		return this.pan;
	}
	
	public ArrayList<DefaultMutableTreeNode> getList(){
		return list;
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == this.rechercher){
			this.list.clear();
			ResultSet rs1 = null;
			Connexion con = new Connexion("ProjetMode.db");
			con.connect();
			
			if(!text_motCle.getText().equals("") && !text_nbPoints.getText().equals("")){
				
				rs1 = con.querySelect("SELECT nom FROM figures WHERE  nom = (SELECT figure FROM association WHERE mot = '"+ text_motCle.getText() +"') AND nbPoints = '" + text_nbPoints.getText() + "'");
			}
			else if(!text_motCle.getText().equals("")){
				rs1 = con.querySelect("SELECT nom FROM figures WHERE  nom = (SELECT figure FROM association WHERE mot = '"+ text_motCle.getText() +"')");
			}
			else if(!text_nbPoints.getText().equals("")){
				rs1 = con.querySelect("Select nom from figures where nbPoints = '" + text_nbPoints.getText() + "'");
			}
			
			try {
				if(rs1 != null){
					while(rs1.next()){
						list.add(new DefaultMutableTreeNode(rs1.getString("nom")));
					}
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			} finally {
				con.close();
				pg.updateTreeRecherche(list);
				pg.revalidate();
				this.dispose();
				fen.setEnabled(true);
			}
		}
	}
}