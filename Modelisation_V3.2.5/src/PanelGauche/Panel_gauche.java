package PanelGauche;

import fenetre.Fenetre;
import gestion_figure.Figure;
import gestion_figure.Panel_Figure;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import loading.LoadFigure;

import bdd.Connexion;

/**
 * Le panel de gauche est le panel qui permet de gerer la base de donnee ainsi que la recherche
 * @author Remy
 *
 */

public class Panel_gauche extends JPanel implements TreeSelectionListener, CaretListener, KeyListener, ActionListener {
	private JTree jt;
	private ArrayList<DefaultMutableTreeNode> list = new ArrayList<>();
	private ArrayList<DefaultMutableTreeNode> listRecherche = new ArrayList<>();
	private Fenetre fen;
	private DefaultMutableTreeNode racine;
	private JTextField recherche = new JTextField("Rechercher ...");
	private JButton bouton_reset = new JButton("Reset");
	private JButton bouton_rechercheAvancee = new JButton("Avancee");
	private LoadFigure loader = new LoadFigure();
	private Box boxRecherche = Box.createVerticalBox();
	private Box boxBouton = Box.createHorizontalBox();

	public Panel_gauche(Fenetre fen) {
		this.fen = fen;
		this.setPreferredSize(new Dimension(200, 0));
		this.setLayout(new BorderLayout());
		this.bouton_reset.addActionListener(this);
		this.bouton_rechercheAvancee.addActionListener(this);
		this.recherche.addKeyListener(this);
		this.recherche.addCaretListener(this);
		
		remplirTree();
	}
	
	/**
	 * RemplirTri est une fonction qui permet tout simplement de remplir l'arbre avec tous les elements de la base de donnee
	 */

	private void remplirTree() {
		Connexion con = new Connexion("ProjetMode.db");
		con.connect();

		racine = new DefaultMutableTreeNode("base de donnee");
		
		ResultSet rs = con.querySelect("select nom from figures order by nom");

		try {
			while (rs.next()) {
				list.add(new DefaultMutableTreeNode(rs.getString("nom")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			for (DefaultMutableTreeNode l : list) {
				racine.add(l);
			}

			jt = new JTree(racine);
			jt.addTreeSelectionListener(this);
			this.add(jt, BorderLayout.NORTH);

			boxBouton.add(bouton_reset);
			boxBouton.add(bouton_rechercheAvancee);
			boxRecherche.add(boxBouton);
			boxRecherche.add(recherche);

			this.add(boxRecherche, BorderLayout.SOUTH);

			con.close();
		}
	}
	
	/**
	 * RemplirTriRecherche est une fonction qui permet de remplir l'arbre avec seulement les elements correspondant a la recherche de l'utilisateur
	 */

	private void remplirTreeRecherche(ArrayList<DefaultMutableTreeNode> l){
		racine = new DefaultMutableTreeNode("base de donnee");
		
		for (DefaultMutableTreeNode l2 : l) {
			racine.add(l2);
		}
		
		if(l.isEmpty()){
			racine.add(new DefaultMutableTreeNode("Aucun Resultat"));
		}
		
		jt = new JTree(racine);
		jt.addTreeSelectionListener(this);
		add(jt, BorderLayout.NORTH);
		
		boxBouton.add(bouton_reset);
		boxBouton.add(bouton_rechercheAvancee);
		boxRecherche.add(boxBouton);
		boxRecherche.add(recherche);;

		this.add(boxRecherche, BorderLayout.SOUTH);
		this.add(recherche, BorderLayout.SOUTH);
	}
	
	/**
	 * Met a jour l'arbre avec les elements de la base de donnee
	 */

	public void updateTree() {
		this.removeAll();
		list.clear();
		remplirTree();
	}
	
	/**
	 * Met a jour l'arbre avec les resultats de la recherche
	 */

	public void updateTreeRecherche(ArrayList<DefaultMutableTreeNode> l){
		this.removeAll();
		remplirTreeRecherche(l);
	}

	public JTree getJt() {
		return jt;
	}

	@Override
	public void valueChanged(TreeSelectionEvent e) {
		TreePath myPath = jt.getSelectionPath();

		if (!(myPath == null)) {
			Connexion conbis = new Connexion("ProjetMode.db");
			conbis.connect();
			
			ResultSet rs2 = conbis.querySelect("Select * from figures where nom = '" + myPath.getLastPathComponent().toString() + "'");

			try {
				fen.setPanelFigure(loader.creerFigure(rs2.getString("chemin")), rs2.getString("chemin"), rs2.getString("nom"));
				fen.repaint();
			} catch (SQLException e1) {
				e1.printStackTrace();
			} finally {
				conbis.close();
			}
		}
	}

	@Override
	public void caretUpdate(CaretEvent e) {
		if(e.getSource() == recherche && recherche.getText().equals("Rechercher ...")) {
			recherche.setText(null);
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			Connexion conRecherche = new Connexion("ProjetMode.db");
			conRecherche.connect();

			listRecherche.clear();

			ResultSet rs = conRecherche.querySelect("select nom from figures where nom = '" + recherche.getText() + "'");
			
			try {
				while(rs.next()){
					listRecherche.add(new DefaultMutableTreeNode(rs.getString("nom")));
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			} finally {
				updateTreeRecherche(listRecherche);
				this.revalidate();
				conRecherche.close();
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == bouton_reset){
			updateTree();
			this.revalidate();
		}
		if(e.getSource() == bouton_rechercheAvancee){
			new FenRechercheAvancee(fen, this);
		}
	}
}
