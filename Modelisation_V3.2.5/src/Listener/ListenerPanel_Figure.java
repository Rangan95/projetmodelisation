package Listener;

import gestion_figure.Panel_Figure;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

/**
 * Permet de gerer les actions sur la figure par la souris et le clavier
 * 
 * @author remy
 *
 */

public class ListenerPanel_Figure implements MouseWheelListener,
		MouseMotionListener, MouseListener {

	private Panel_Figure panelFigure;
	// variables qui mémorisent les coordonnées de la souris au moment du
	// click
	private int mouseGetX;
	private int mouseGetY;
	// variable qui mémorise le type de bouton de la souris
	private int buttonType;

	public ListenerPanel_Figure(Panel_Figure panelFigure) {
		this.panelFigure = panelFigure;
		this.panelFigure.setFocusable(true);
		this.panelFigure.requestFocus();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		buttonType = e.getButton();
		mouseGetX = e.getX();
		mouseGetY = e.getY();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseDragged(MouseEvent e) {
		int mouseGetX2 = mouseGetX - e.getX();
		int mouseGetY2 = mouseGetY - e.getY();

		if(this.panelFigure.getFigure().isRestreindreDeplacement() == false){
			if (mouseGetY2 < 0 && buttonType == 3) {
				panelFigure.getFigure().rotationX(-this.panelFigure.getFigure().getRotationSensi());
			}
	
			if (mouseGetY2 > 0 && buttonType == 3) {
				panelFigure.getFigure().rotationX(this.panelFigure.getFigure().getRotationSensi());
			}
	
			if (mouseGetX2 < 0 && buttonType == 3) {
				panelFigure.getFigure().rotationY(-this.panelFigure.getFigure().getRotationSensi());
			}
	
			if (mouseGetX2 > 0 && buttonType == 3) {
				panelFigure.getFigure().rotationY(this.panelFigure.getFigure().getRotationSensi());
			}
			
			if (mouseGetY2 < 0 && buttonType == 1) {
				this.panelFigure.getFigure().translation(0,-(panelFigure.getFigure().getBarycentreFigure().getY() - e.getY()));
			}
			if (mouseGetY2 > 0 && buttonType == 1) {
				this.panelFigure.getFigure().translation(0,-(panelFigure.getFigure().getBarycentreFigure().getY() - e.getY()));
			}
			if (mouseGetX2 < 0 && buttonType == 1) {
				this.panelFigure.getFigure().translation(-(panelFigure.getFigure().getBarycentreFigure().getX() - e.getX()), 0);
			}
			if (mouseGetX2 > 0 && buttonType == 1) {
				this.panelFigure.getFigure().translation(-(panelFigure.getFigure().getBarycentreFigure().getX() - e.getX()), 0);
			}
		}

		mouseGetX = e.getX();
		mouseGetY = e.getY();
		panelFigure.repaint();
	}

	@Override
	public void mouseMoved(MouseEvent e) {

	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		if(this.panelFigure.getFigure().isZoomRestreint() == false){
			if (e.getWheelRotation() > 0) {
				this.panelFigure.getFigure().zoom(1.1);
			}
	
			if (e.getWheelRotation() < 0) {
				this.panelFigure.getFigure().zoom(0.9);
			}
		}
		panelFigure.repaint();
	}
}
